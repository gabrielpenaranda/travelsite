<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>ViajesLunaLLena</title>
<!--<link rel="shortcut icon" type="image/x-icon" href="imagenes/favicon.png" />-->
<link rel="stylesheet" type="text/css" href="style.css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Amaranth' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="style/js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="style/js/ddsmoothmenu.js"></script>
<script type="text/javascript" src="style/js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="style/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="style/js/carousel.js"></script>
<script type="text/javascript" src="style/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="style/js/jquery.masonry.min.js"></script>
<script type="text/javascript" src="style/js/jquery.slickforms.js"></script>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<!--<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">-->
</head>
<body>
<!-- Begin Wrapper -->
<div id="wrapper">

	<h1 class="title"><img src="imagenes/logos/logobanner.png" align="left" alt="" /></h1><br /><br /><br /><br />
    <!--<div class="line"></div> -->
	<div class="intro"><p>Los Mejores Destinos para su Descanso</p></div>

      <div id="sidebar">

        <!-- Begin Menu -->
        <div id="menu" class="menu-v">
          <ul>
            <li><a href="index.php" class="active">Inicio </a></li>
            <li><a href="nuestra_empresa.php">Nuestra Empresa</a></li>
            <li><a href="destinosTuristicos.php">Destinos Turísticos</a></li>
            <li><a href="otroservicios.php">Otros Servicios</a></li>
            <li><a href="contacto.php">Contacto</a></li>
          </ul>

        </div>
		<?php include("redes.php");?>


      </div>
  <!-- Begin Content -->
  		<div id="content">
	<!--	<!-- Begin Slider -->
            <div id="slider">

                <div class="flexslider">

                <ul class="slides">
                    <span>que es esto</span>
                    <li><?php include('enlaces.php'); ?><img src="imagenes/slides/figura1.jpg" alt=""/></a></li>
                    <li><?php include('enlaces2.php'); ?><img src="imagenes/slides/figura2.jpg" alt=""/></a></li>
                    <li><img src="imagenes/slides/figura4.jpg" alt=""/></li>
		                <li><?php include('enlaces2.php'); ?><img src="imagenes/slides/figura6.jpg" alt=""/></a></li>
                </ul>
              </div>

            </div>
            <!-- End Slider -->


            <div class="one-half">
            <h3><?php include('enlaces.php'); ?><img src="imagenes/vuelos.png"   alt="" /></a></h3>
            </div>

            <div class="one-half last">
            <h3><?php include('enlaces2.php'); ?><img src="imagenes/hoteles.png"   alt="" /></a></h3>

            </div>
            <div class="one-half last">
            <h3><?php include('enlaces2.php'); ?><img src="imagenes/vehiculos.png"   alt="" /></h3>

            </div>

            <div class="clear"></div><br><br><br><br>




		   <?php include_once ("footer.php") ?>
            <div class="map">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d1964.196179805673!2d-69.32466850091159!3d10.066901438919086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e6!4m3!3m2!1d10.066761470418363!2d-69.3236707192525!4m5!1s0x8e876705586386c7%3A0xa8e66c8b84266f2f!2sCalle+32%2C+Barquisimeto+3001!3m2!1d10.067032!2d-69.32352759999999!5e0!3m2!1ses-419!2sve!4v1414360077492" width="600" height="200" frameborder="0" style="border:0"></iframe>
                            <p><br />
                             <small>
                                <br>
                           <a href="https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=10.063431,-69.315941&amp;spn=0.005282,0.008583&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">Ver mapa más grande</a>
                            </small>
                            </p>

            </div>



	</div>
	<!-- End Content -->

</div>
<!-- End Wrapper -->
<div class="clear"></div>
<script type="text/javascript" src="style/js/scripts.js"></script>
<script type="text/javascript" src="style/js/jquery.corner.js"></script>
<script type="text/javascript">
</script>
</body>
</html>

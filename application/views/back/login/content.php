<div class="container">
    <div class="row">
        <div class="col-xs-offset-4 col-xs-4 login">
            <form action="<?php base_url() ?>verifica" method="post">
                <div class="form-group">
                    <label for="usuario">Usuario</label>
                    <input type="usuario" class="form-control" id="usuario" name="usuario" placeholder="Usuario">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-default">Ingresar</button>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-5">
            <h3>Registro de Ciudades</h3>
        </div>

        <div class="col-xs-offset-3 col-xs-5">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#consulta" aria-controls="profile" role="tab" data-toggle="tab">CONSULTA</a></li>
                <li role="presentation"><a href="#registro" aria-controls="home" role="tab" data-toggle="tab">REGISTRO</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane" id="consulta">
                    <table class="table table-striped">
                        <thead>
                            <th>Ciudad</th>
                            <th>País</th>
                            <th><center>Acciones</center></th>
                        </thead>
                        <tbody>
                            <?php
                            foreach($listado->result() as $p):?>
                                <tr>
                                    <td><?php echo $p->descripcion; ?></td>
                                    <td><?php echo $p->pais; ?></td>
                                    <td>
                                       <center>
                                           <a href="<?php echo base_url('ciudad/eliminar/').$p->id; ?>" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                           <a href="<?php echo base_url('ciudad/editar/').$p->id; ?>" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                       </center>
                                    </td>
                                </tr>
                            <?php
                            endforeach;?>
                        </tbody>
                    </table>


                </div>
                <div role="tabpanel" class="tab-pane active" id="registro">
                    <?php
                    echo validation_errors("<p style='color:red;'>","</p>");
                    ?>

                        <form method="post" action="<?php echo base_url('ciudad/valida') ?>">

                            <div class="form-group">
                                <label for="ciudad">Ciudad: </label>
                                <input type="text" class="form-control" id="ciudad" name="txtCiudad" placeholder="Ciudad">
                            </div>

                            <div class="form-group">
                                <label for="pais">Pais: </label>
                                <select class="form-control" name="cmbPais">
                                <?php
                                foreach($consulta->result() as $p):
                                    if ($p->descripcion=='Venezuela'){
                                        echo '<option selected="selected" >'.$p->descripcion.'</option>';
                                    }
                                    else{
                                        echo '<option>'.$p->descripcion.'</option>';
                                    }
                                endforeach;
                                ?>
                                </select>
                            </div>

                            <input type="submit" class="btn btn-primary" value="Guardar" name="guardar">

                        </form>
                </div>


            </div>
        </div>

    </div>
</div>


<!--
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="#">Home</a></li>
  <li role="presentation"><a href="#">Profile</a></li>
  <li role="presentation"><a href="#">Messages</a></li>
</ul>-->

<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-6">
            <h3><center>Registro de Promociones</center></h3>
        </div>

        <div class="col-xs-offset-1 col-xs-10">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#consulta" aria-controls="profile" role="tab" data-toggle="tab">CONSULTA</a></li>
                <li role="presentation"><a href="#registro" aria-controls="home" role="tab" data-toggle="tab">REGISTRO</a></li>
            </ul>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane" id="consulta">
                    <table class="table table-striped">
                        <thead>
                            <th>Promocion</th>
                            <th>Descripción</th>
                            <th><center>Fecha Inicio</center></th>
                            <th><center>Fecha Fin</center></th>
                            <th><center>Status</center></th>
                            <th><center>Imágen</center></th>
                            <th><center>Acciones</center></th>
                        </thead>
                        <tbody>
                            <?php
                            if ($listado != NULL):
                                foreach($listado->result() as $p):?>
                                    <tr>
                                        <td><?php echo $p->nombre; ?></td>
                                        <td><?php echo $p->descripcion; ?></td>
                                        <td><center><?php echo substr($p->fecha_inicio,8,2).'/'.substr($p->fecha_inicio,5,2).'/'.substr($p->fecha_inicio,0,4); ?></center></td>
                                        <td><center><?php echo substr($p->fecha_fin,8,2).'/'.substr($p->fecha_fin,5,2).'/'.substr($p->fecha_fin,0,4); ?></center></td></td>
                                        <td><center><?php
                                            if ($p->status):
                                                echo 'Activo';
                                            else:
                                                echo 'Inactivo';
                                            endif; ?></center></td>
                                        <td><center><img src="<?php echo base_url().'uploads/thumbs/promocion/'.$p->thumb ?>" alt=""></center></td>
                                        <td>
                                           <center>
                                               <a href="<?php echo base_url('promociones-back/elimina/').$p->id; ?>" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                               <a href="<?php echo base_url('promociones-back/edita/').$p->id; ?>" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                               <a href="<?php echo base_url('promociones-back/imagen/').$p->id; ?>" title="Cambiar imágen"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a>
                                           </center>
                                        </td>
                                    </tr>
                            <?php
                                endforeach;
                            endif;?>
                        </tbody>
                    </table>
                </div>

                <div role="tabpanel" class="tab-pane active" id="registro">

                    <?php
                    echo validation_errors("<div class='alert alert-danger'>","</div>");

                    echo form_open_multipart(base_url('promociones-back/registro'));

                    echo '<div class="form-group">';
                    echo form_label('Promocion:', "nombre");
                    echo form_input($campos['nombre']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Descripción:', "descripcion");
                    echo form_textarea($campos['descripcion']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Incluye:', "siincluye");
                    echo form_textarea($campos['siincluye']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('No incluye:', "noincluye");
                    echo form_textarea($campos['noincluye']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Fecha inicio:', "fecha_inicio");
                    echo form_input($campos['fecha_inicio']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Fecha final:', "fecha_fin");
                    echo form_input($campos['fecha_fin']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Precio:', "precio");
                    echo form_input($campos['precio']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Status:', "status");
                    echo form_dropdown($campos['status']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Imágen:', "imagen");
                    echo form_upload($campos['imagen']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_submit($campos['guardar']);
                    echo '</div>';

                    echo form_close();
                    ?>

                </div>


            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $('#<?php echo $campos['fecha_inicio']['id'] ?>').datepicker();
        $('#<?php echo $campos['fecha_fin']['id'] ?>').datepicker();
    });
</script>

<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-6">
            <center><h4>Registro de Post</h4><small>Modificar Datos</small></center>
        </div>

        <div class="col-xs-offset-1 col-xs-10">


            <?php
            $campos['nombre']['value'] = $listado->nombre;
            $campos['descripcion']['value'] = $listado->descripcion;
            $campos['contenido']['value'] = $listado->contenido;

            echo validation_errors("<div class='alert alert-danger'>","</div>");

            echo form_open_multipart(base_url('blog-back/editado').'/'.$listado->id);

            echo '<div class="form-group">';
            echo form_label('Destino:', "nombre");
            echo form_input($campos['nombre']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Descripción:', "descripcion");
            echo form_textarea($campos['descripcion']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Contenido:', "contenido");
            echo form_textarea($campos['contenido']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_submit($campos['guardar']);
            echo '</div>';

            echo form_close();
            ?>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-6">
            <center><h4>Registro de Post</h4><small>Modificar Imágen</small></center><br>

        </div>

        <div class="col-xs-offset-3 col-xs-6">

            <?=@$error?>

            <?php
            $t = $listado->thumb;

            echo validation_errors("<div class='alert alert-danger'>","</div>");

            echo form_open_multipart(base_url('blog-back/cambia_imagen').'/'.$listado->id);

            echo '<div class="form-group">';
            echo form_label('Imágen:', "imagen");
            echo '<br>';
            echo '<img src="'.base_url().'uploads/thumbs/blog/'.$t.'" />';
            echo '<br><br>';
            echo form_upload($campos['imagen']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_submit($campos['guardar']);
            echo '</div>';

            echo form_close();
            ?>

        </div>
    </div>
</div>

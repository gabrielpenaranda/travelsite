<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-6">
            <center><h4>Registro de Paquetes</h4><small>Modificar Datos</small></center>
        </div>

        <div class="col-xs-offset-1 col-xs-10">


            <?php
            $campos['nombre']['value'] = $listado->nombre;
            $campos['descripcion']['value'] = $listado->descripcion;
            $campos['siincluye']['value'] = $listado->si_incluye;
            $campos['noincluye']['value'] = $listado->no_incluye;
            $fechai = substr($listado->fecha_inicio,8,2).'/'.substr($listado->fecha_inicio,5,2).'/'.substr($listado->fecha_inicio,0,4);
            $campos['fecha_inicio']['value'] = $fechai;
            $fechaf = substr($listado->fecha_fin,8,2).'/'.substr($listado->fecha_fin,5,2).'/'.substr($listado->fecha_fin,0,4);
            $campos['fecha_fin']['value'] = $fechaf;
            $campos['precio']['value'] = $listado->precio;
            $campos['status']['selected'] = $listado->status;
            // if ($listado->status):
            //     $campos['status']['selected'] = 'Activo';
            // else:
            //     $campos['tipo']['selected'] = 'Inactivo';
            // endif;

            echo validation_errors("<div class='alert alert-danger'>","</div>");

            echo form_open_multipart(base_url('paquetes-back/editado').'/'.$listado->id);

            echo '<div class="form-group">';
            echo form_label('Destino:', "nombre");
            echo form_input($campos['nombre']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Descripción:', "descripcion");
            echo form_textarea($campos['descripcion']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Incluye:', "siincluye");
            echo form_textarea($campos['siincluye']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('No incluye:', "noincluye");
            echo form_textarea($campos['noincluye']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Fecha inicio:', "fecha_inicio");
            echo form_input($campos['fecha_inicio']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Fecha final:', "fecha_fin");
            echo form_input($campos['fecha_fin']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Precio:', "precio");
            echo form_input($campos['precio']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Status:', "status");
            echo form_dropdown($campos['status']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_submit($campos['guardar']);
            echo '</div>';

            echo form_close();
            ?>

        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        $('#<?php echo $campos['fecha_inicio']['id'] ?>').datepicker();
        $('#<?php echo $campos['fecha_fin']['id'] ?>').datepicker();
    });
</script>

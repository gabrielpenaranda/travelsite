   <footer>
        <div class="container-fluid">
            <div class="row pie">
                <div class="col-xs-12">
                    <!-- <br><br><br><br> -->
                    <h5>&copy; 2016 Páginas WEB www.paginas-web.com.ve</h5>
                </div>
            </div>
        </div>
    </footer>

    <script src="<?php echo base_url() ?>resources/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>resources/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>resources/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>resources/js/main.js"></script>
    <script src="<?php echo base_url() ?>resources/js/carousel-fade.js"></script>

    <script>
        $(document).ready(function(){
            $(‘.datepicker’).datepicker();
        });
    </script>

</body>

</html>

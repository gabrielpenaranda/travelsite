<div class="container">
    <div class="row">

        <div class="col-xs-offset-3 col-xs-6">
            <h3><center>Registro de Destinos</center></h3>
        </div>

        <div class="col-xs-offset-1 col-xs-10">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="#consulta" aria-controls="profile" role="tab" data-toggle="tab">CONSULTA</a></li>
                <li role="presentation"><a href="#registro" aria-controls="home" role="tab" data-toggle="tab">REGISTRO</a></li>
            </ul>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane" id="consulta">
                    <table class="table table-striped">
                        <thead>
                            <th><center>Destino</center></th>
                            <th><center>Descripción</center></th>
                            <th><center>Tipo Destino</center></th>
                            <th><center>Status</center></th>
                            <th><center>Imágen</center></th>
                            <th><center>Acciones</center></th>
                        </thead>
                        <tbody>
                            <?php
                            if ($listado != NULL):
                                foreach($listado->result() as $p):?>
                                    <tr>
                                        <td><?php echo $p->nombre; ?></td>
                                        <td><?php echo $p->descripcion; ?></td>
                                        <td><?php echo $p->tipo; ?></td>
                                        <td><?php
                                            if ($p->status):
                                                echo 'Activo';
                                            else:
                                                echo 'Inactivo';
                                            endif; ?></td>
                                        <td><img src="<?php echo base_url().'uploads/thumbs/destino/'.$p->thumb ?>" alt=""></td>
                                        <td>
                                           <center>
                                               <a href="<?php echo base_url('destinos-back/elimina/').$p->id; ?>" title="Eliminar"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                               <a href="<?php echo base_url('destinos-back/edita/').$p->id; ?>" title="Editar"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                               <a href="<?php echo base_url('destinos-back/imagen/').$p->id; ?>" title="Cambiar imágen"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a>
                                           </center>
                                        </td>
                                    </tr>
                            <?php
                                endforeach;
                            endif;?>
                        </tbody>
                    </table>
                </div>

                <div role="tabpanel" class="tab-pane active" id="registro">

                    <?php
                    echo validation_errors("<div class='alert alert-danger'>","</div>");

                    echo form_open_multipart(base_url('destinos-back/registro'));

                    echo '<div class="form-group">';
                    echo form_label('Destino:', "nombre");
                    echo form_input($campos['nombre']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Descripción:', "descripcion");
                    echo form_textarea($campos['descripcion']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Tipo de destino:', "tipo");
                    echo form_dropdown($campos['tipo']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Status:', "status");
                    echo form_dropdown($campos['status']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_label('Imágen:', "imagen");
                    echo form_upload($campos['imagen']);
                    echo '</div>';

                    echo '<div class="form-group">';
                    echo form_submit($campos['guardar']);
                    echo '</div>';

                    echo form_close();
                    ?>

                </div>


            </div>
        </div>

    </div>
</div>

     <div class="container-fluid">
        <div class="row header-back">
            <div class="col-xs-12">

                <header>
                    <img class="img-responsive center-block img-header-back" src="<?php echo base_url()?>resources/img/logo_nuevo_centrado2.png" alt="CORTURLIFE... Turismo de Vida">
                </header>
            </div>
            <div class="col-xs-12">

                <nav class="navbar navbar-default center-block">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsed-bar-1" aria-expanded="false">
                <span class="sr-only">Menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                       <a class="navbar-brand" href="#">TravelSite</a>
                    </div>
                    <?php if ($this->session->userdata('login')): ?>

                    <div class="collapse navbar-collapse" id="collapsed-bar-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url('destinos-back') ?>">DESTINOS</a></li>
                            <li><a href="<?php echo base_url('paquetes-back') ?>">PAQUETES</a></li>
                            <li><a href="<?php echo base_url('promociones-back') ?>">PROMOCIONES</a></li>
                            <li><a href="<?php echo base_url('blog-back') ?>">BLOG</a></li>
                            <li><a href="<?php echo base_url('logout') ?>">SALIR</a></li>
                        </ul>
                    </div>

                  <?php else: ?>
                          <div class="collapse navbar-collapse" id="collapsed-bar-1">
                            <ul class="nav navbar-nav">
                              <li><a href="<?php echo base_url() ?>admin">INICIAR SESIÓN</a></li>
                            </ul>
                         </div>
                  <?php endif; ?>

                </nav>
            </div>
        </div>

      </div>

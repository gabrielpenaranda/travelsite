<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CORTURLIFE-Turismo de Vida</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/app-back.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/btstrp-back.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.structure.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.theme.css">
    <script src="<?php echo base_url() ?>resources/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>resources/jquery-ui/jquery-ui.js"></script>
    <script src="<?php echo base_url() ?>resources/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>resources/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>resources/js/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        themes: "modern",
        language : "es",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
    </script>
</head>

<body>

<section id="enlaces">
    <div class="container">
        <div class="row fondo-row-info">
            <div class="col-xs-offset-1 col-xs-10">
                <br>
                <p class="resaltado-titulo text-center"><?php echo $listado->nombre ?></p>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                <p class="resaltado-2 text-center"><?php echo nl2br($listado->descripcion) ?></p>
            </div>
            <div class="col-xs-12">
                <center> <img src="<?php echo base_url().'uploads/images/blog/'.$listado->imagen ?>" class="img-responsive" alt=""> </center>
                <br>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                <p class="resaltado-info text-left"><?php echo nl2br($listado->contenido) ?></p>
                <br><br>
            </div>
        </div>
    </div>
</section>

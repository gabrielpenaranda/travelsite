<section id="enlaces">
    <div class="container">
        <div class="row equal-height">
            <div class="col-xs-12">
                <br>
                <p class="resaltado-titulo text-center">BLOG</p>
            </div>
            <?php
            if ($listado == NULL)
            {
                echo '<div class="col-xs-12">';
                echo '<h3 class="alert alert-success text-center">'.'No se encontraron posts'.'</h3>';
                echo '</div>';
            }
            else
            {
                $i = 0;
                foreach ($listado->result() as $l)
                {
                    $id[$i] = $l->id;
                    $thumb[$i] = $l->thumb;
                    $nombre[$i] = $l->nombre;
                    $descripcion[$i] = $l->descripcion;
                    $i++;
                }
                for ($i = $numero-1; $i >= 0; $i--)
                {
                    echo '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">';
                    echo '<div class="thumbnail gr-color-1">';
                    echo '<a href="'.base_url('blog/info/').$id[$i].'"><img src="'. base_url().'uploads/thumbs/blog/'.$thumb[$i].'" alt="'.$nombre[$i].'"></a>';
                    echo '<div class="caption">';
                    echo '<h3 class="resaltado-2 color-2 flex-text">'.$nombre[$i].'</h3>';
                    echo '<p class="resaltado-1 color-2 flex-text">'.$descripcion[$i].'</p>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                }
            }
            ?>
            <div class="col-xs-12"><br><br></div>
        </div>
    </div>
</section>

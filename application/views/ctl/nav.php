<div class="container">
    <div class="row">
        <div class="col-xs-12 img-header">
            <header>
                <img class="img-responsive center-block " src="<?php echo base_url()?>resources/img/header-200.jpg" alt="TravelSite-Sistema de Agencias de Viajes y Turismo">
                <hgroup>
                    <h1 class="oculto">GEOS Tecnología</h1>
                    <h2 class="oculto">TravelSite</h2>
                    <h2 class="oculto">Sistema</h2>
                    <h3 class="oculto">Agencia de Viajes y Turismo</h3>
                </hgroup>
            </header>
        </div>
        <div class="col-xs-12 header">
            <nav class="navbar navbar-default center-block">
               <hgroup>
                   <h2 class="oculto">Menu Principal</h2>
               </hgroup>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsed-bar-1" aria-expanded="false">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="collapsed-bar-1">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo base_url() ?>">INICIO</a></li>
                        <li><a href="<?php echo base_url('somos') ?>">TRAVELSITE</a></li>
                        <li><a href="<?php echo base_url('destinos') ?>">DESTINOS</a></li>
                        <li><a href="<?php echo base_url('paquetes') ?>">PAQUETES</a></li>
                        <li><a href="<?php echo base_url('promociones') ?>">PROMOCIONES</a></li>
                        <li><a href="<?php echo base_url('blog') ?>">BLOG</a></li>
                        <li><a href="<?php echo base_url('contacto') ?>">CONTACTO</a></li>
                    </ul>
                </div>

            </nav>
        </div>
    </div>

</div>

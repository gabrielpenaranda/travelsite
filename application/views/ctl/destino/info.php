<section id="enlaces">
    <div class="container">
        <div class="row fondo-row-info">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <br>
                <h2 class="resaltado-titulo text-center"><?php echo $listado->nombre ?></h2>
            </div>
            <div class="col-xs-12">
                <center> <img src="<?php echo base_url().'uploads/images/destino/'.$listado->imagen ?>" class="img-responsive" alt="Destino"> </center>
                <br>
            </div>
            <div class="col-xs-offset-1 col-xs-10">
                <p class="resaltado-2 text-left"><?php echo nl2br($listado->descripcion) ?></p>
                <br><br>
            </div>
        </div>
    </div>
</section>

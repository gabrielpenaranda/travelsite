<section id="enlaces">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <center>
                    <br>
                    <h2 class="resaltado-titulo">DESTINOS</h2>
                </center>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="thumbnail gr-color-3">
                    <a href="<?php echo base_url('destinos/listado/1') ?>"><img src="<?php echo base_url('resources/img/').'nacional.png' ?>" alt="CORTURLIFE"></a>
                    <div class="caption">
                        <h3 class="resaltado-titulo color-1">NACIONALES</h3>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="thumbnail gr-color-6">
                    <a href="<?php echo base_url('destinos/listado/2') ?>"><img src="<?php echo base_url('resources/img/').'internacional.png' ?>" alt="DESTINOS"></a>
                    <div class="caption">
                        <h3 class="resaltado-titulo color-1">INTERNACIONALES</h3>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <br><br>
            </div>
        </div>
    </div>
</section>

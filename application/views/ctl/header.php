<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Agencia de viajes y turismo, paquetes de viajes, promociones de viajes, destinos nacionales, destinos internacionales, paquetes, promociones, destinos, viaje, turismo">
    <meta name="keywords" content="travelsite, agencia, viajes, turismo, destinos, paquetes, promociones, nacionales, internacionales">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TravelSite-Sistema de Agencias de Viajes</title>
    <link href='http://fonts.googleapis.com/css?family=Amaranth' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/coloresinicio.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/app.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/btstrp.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>resources/css/fade.css">
</head>

<body>

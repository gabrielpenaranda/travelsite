<section id="enlaces">
    <div class="container">
        <div class="row">
            <!-- <div class="clearfix"></div> -->
            <div class="col-xs-12 fondo-row-tr">
                <br>
            </div>
            <div class="col-xs-12">
                <div class="thumbnail gr-color-6">
                    <!-- <a href="#"><img src="http://placehold.it/250x250" alt="MISION"></a> -->
                    <div class="row">
                        <div class="col-xs-offset-2 col-xs-8">
                            <div class="caption">
                                <br>
                                <h3 class="resaltado-titulo color-1">Que es TravelSite</h3><br>
                                <p class="resaltado-3 text-left color-1">TravelSite es un sistema de información dirigido al segmento de Agencias de Viajes y Turismo.</p>
                                <p class="resaltado-3 text-left color-1">TravelSite permite controlar efectivamente la información  relativa a destinos, paquetes y promociones de viajes y turismo. Asimismo, cuenta con un blog en el que se opueden agregar articulos de interes general o relacionados a viajes.</p>
                                <br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="thumbnail gr-color-8">
                    <!-- <a href="#"><img src="http://placehold.it/250x250" alt="VISION"></a> -->
                    <div class="row">
                        <div class="col-xs-offset-2 col-xs-8">
                            <div class="caption">
                                <br>
                                <h3 class="resaltado-titulo color-1">VISION</h3><br>
                                <p class="resaltado-3 text-left color-1">SER LA ORGANIZACIÓN LIDER EN CALIDAD DE SERVICIO TURISTICO, QUE PROYECTE UNA IMAGEN PROFESIONAL CON CAPACIDAD PARA ATENDER Y ENFRENTAR LOS CAMBIOS CONSTANTES EN EL AREA</p>
                                <br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="thumbnail gr-color-7">
                    <!-- <a href="#"><img src="http://placehold.it/250x250" alt="POLITICAS DE CALIDAD"></a> -->
                    <div class="row">
                        <div class="col-xs-offset-2 col-xs-8">
                            <div class="caption">
                                <br>
                                <h3 class="resaltado-titulo color-1">POLITICAS DE CALIDAD</h3><br>
                                <p class="resaltado-3 text-left color-1">TRABAJAR PARA CUMPLIR PERMANENTEMENTE CON LAS NECESIDADES Y EXPECTATIVAS DE NUESTROS CLIENTES, OFRECIENDO LAS MEJORES ALTERNATIVAS DE SERVICIOS TURÍSTICOS DE MANERA OPORTUNA, INFORMACIÓN CONFIABLE Y ACTITUD DE SERVICIO QUE GARANTICEN SU SATISFACCIÓN.</p>
                                <p class="resaltado-info text-left color-1">LA ATENCIÓN PERSONALIZADA DE NUESTRO EQUIPO COMERCIAL MARCA LA DIFERENCIA UNIDA A NUESTRA UNIDAD DE NEGOCIOS FREELANCE DONDE BUSCAMOS LOS MAYORES BENEFICIOS.</p>
                                <br>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 fondo-row-tr">
                <center>
                    <br>
                    <h2 class="resaltado-titulo">NUESTRO EQUIPO</h2>
                </center>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail gr-color-2">
                    <img src="<?php echo base_url('resources/img/somos/').'corturlife.png' ?>" alt="DESTINOS">
                    <div class="caption">
                        <h3 class="resaltado-2 color-1">NICNAR QUERO</h3>
                        <h4 class="resaltado-2 color-1">GERENTE GENERAL</h4>
                        <h5 class="resaltado-2 color-1">+58 424-5144657 <img src="<?php echo base_url('resources/img/somos/').'iconowhatsap.png' ?>" alt="Icono de Whatsapp"></h5>
                        <h5 class="resaltado-1 color-1">grupocorturlifeca@turismodevida.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail gr-color-5">
                    <img src="<?php echo base_url('resources/img/somos/').'yoha.jpg' ?>" alt="DESTINOS">
                    <div class="caption">
                        <h3 class="resaltado-2 color-1">JOHANNY DUNO</h3>
                        <h4 class="resaltado-2 color-1">ESPECIALISTA EN VIAJES</h4>
                        <h5 class="resaltado-2 color-1">+58 424 5144656 <img src="<?php echo base_url('resources/img/somos/').'iconowhatsap.png' ?>" alt="Icono de Whatsapp"></h5>
                        <h5 class="resaltado-1 color-1">corturlife3@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail gr-color-1">
                    <img src="<?php echo base_url('resources/img/somos/').'jenny.jpg' ?>" alt="DESTINOS">
                    <div class="caption">
                        <h3 class="resaltado-2 color-1">JENNIFER DURAN</h3>
                        <h4 class="resaltado-2 color-1">ESPECIALISTA EN VIAJES</h4>
                        <h5 class="resaltado-2 color-1">+58 412 0599980 <img src="<?php echo base_url('resources/img/somos/').'iconowhatsap.png' ?>" alt="Icono de Whatsapp"></h5>
                        <h5 class="resaltado-1 color-1">corturlife2@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="thumbnail gr-color-3">
                    <img src="<?php echo base_url('resources/img/somos/').'enny.jpg' ?>" alt="DESTINOS">
                    <div class="caption">
                        <h3 class="resaltado-2 color-1">ENNYMAR RODRIGUEZ</h3>
                        <h4 class="resaltado-2 color-1">ESPECIALISTA EN VIAJES</h4>
                        <h5 class="resaltado-2 color-1">+58 424 5144663 <img src="<?php echo base_url('resources/img/somos/').'iconowhatsap.png' ?>" alt="Icono de Whatsapp"></h5>
                        <h5 class="resaltado-1 color-1">corturlife1@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 fondo-row-tr">
                <br><br>
            </div>
        </div>
    </div>
</section>

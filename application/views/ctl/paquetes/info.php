<section id="enlaces">
    <div class="container">
        <div class="row fondo-row-info">
            <div class="col-xs-offset-1 col-xs-10">
                <br>
                <p class="resaltado-titulo text-center"><?php echo $listado->nombre ?></p>
                <br>
            </div>
            <div class="col-xs-12">
                <center><img src="<?php echo base_url().'uploads/images/paquete/'.$listado->imagen ?>" class="img-responsive" alt=""></center>
                <br>
            </div>
            <div class="col-xs-offset-2 col-xs-8 text-left">
                <p class="resaltado-2"><?php echo nl2br($listado->descripcion) ?></p>
                <br>
            </div>

            <div class="col-sm-offset-1 col-xs-12 col-sm-2 text-right">
                <p class="resaltado-1">Que incluye este paquete:</p>
            </div>
            <div class="clearfix visible-xs-block"></div>
            <div class="col-xs-12 col-sm-8 text-left">
                <p class="resaltado-info"><?php echo nl2br($listado->si_incluye) ?></p>
            </div>
            <div class="clearfix visible-xs-block"></div>
            <div class="col-sm-offset-1 col-xs-12 col-sm-2 text-right">
                <p class="resaltado-1">Que no incluye este paquete:</p>
            </div>
            <div class="col-xs-12 col-sm-8 text-left">
                <p class="resaltado-info"><?php echo nl2br($listado->no_incluye) ?></p>
            </div>
            <div class="col-sm-offset-1 col-xs-12 col-sm-2 text-right">
                <p class="resaltado-1">Desde:</p>
            </div>
            <div class="col-xs-12 col-sm-9 text-left">
                <p class="resaltado-info"><?php echo substr($listado->fecha_inicio,8,2).'/'.substr($listado->fecha_inicio,5,2).'/'.substr($listado->fecha_inicio,0,4); ?></p>
            </div>
            <div class="col-sm-offset-1 col-xs-12 col-sm-2 text-right">
                <p class="resaltado-1">Hasta:</p>
            </div>
            <div class="col-xs-12 col-sm-9 text-left">
                <p class="resaltado-info"><?php echo substr($listado->fecha_fin,8,2).'/'.substr($listado->fecha_fin,5,2).'/'.substr($listado->fecha_fin,0,4); ?></p>
            </div>
            <div class="col-sm-offset-1 col-xs-12 col-sm-2 text-right">
                <p class="resaltado-1">Precio Bs.F.</p>
            </div>
            <div class="col-xs-12 col-sm-9 text-left">
                <p class="resaltado-info"><?php echo number_format($listado->precio, 2, ',', '.') ?></p>
                <br><br>
            </div>
        </div>
    </div>
</section>

<section id="enlaces">
    <div class="container">
        <div class="row equal-height">
            <div class="col-xs-12">
                <br>
                <p class="resaltado-titulo text-center">PAQUETES</p>
            </div>
            <?php
            if ($listado == NULL)
            {
                echo '<div class="col-xs-12">';
                echo '<h3 class="alert alert-success text-center">'.'No se encontraron paquetes'.'</h3>';
                echo '</div>';
            }
            else
            {
                foreach ($listado->result() as $l)
                {
                    echo '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">';
                    echo '<div class="thumbnail gr-color-1">';
                    echo '<a href="'.base_url('paquetes/info/').$l->id.'"><img src="'. base_url().'uploads/thumbs/paquete/'.$l->thumb.'" alt="'.$l->nombre.'"></a>';
                    echo '<div class="caption">';
                    echo '<h3 class="resaltado-2 color-2 flex-text">'.$l->nombre.'</h3>';
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                }
            }
            ?>
            <div class="col-xs-12"><br><br></div>
        </div>
    </div>
</section>

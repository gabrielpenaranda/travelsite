 <section id="slider">
   <hgroup>
       <h2 class="oculto">Slider</h2>
   </hgroup>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="7"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="<?php echo base_url()?>resources/img/slidermultidestinos1400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/rio_de_janeiro-1400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/slider11400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url('resources/img/salto_angel-1400x480.jpg')?>">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/slider21400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/macchu_picchu-1400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/slider81400x480.jpg">
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url()?>resources/img/slider61400x480.jpg">
                        </div>
                    </div>

                    <!-- Controles -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

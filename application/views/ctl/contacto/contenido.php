<div class="container">
    <div class="row">

        <div class="col-xs-12">
            <br>
            <p class="resaltado-titulo text-center">Contacto</p>
        </div>
        <div class="col-xs-12 col-sm-4">
             <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d1964.196179805673!2d-69.32466850091159!3d10.066901438919086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e6!4m3!3m2!1d10.068975!2d-69.316974!4m5!1s0x8e876705586386c7%3A0xa8e66c8b84266f2f!2sCalle+32%2C+Barquisimeto+3001!3m2!1d10.067032!2d-69.32352759999999!5e0!3m2!1ses-419!2sve!4v1414360077492" width="1200" height="400" frameborder="3" style="border:3"></iframe>
            <p> -->
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1964.1874446442137!2d-69.31676132209013!3d10.068336609167435!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sve!4v1484220978065" width="380" height="497" frameborder="0" style="border:0" allowfullscreen></iframe>
            <small> -->
            <iframe src="https://www.google.com/maps/d/embed?mid=1XsBeXvQdDM3tjNKIkyk2AuC5xIE" width="380" height="497"></iframe>
            <a href="https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=10.068336609167435,-69.31676132209013&amp;spn=0.005282,0.008583&amp;z=16&amp;source=embed" style="color:#0000FF;text-align:left">Ver mapa más grande</a>
                </small>
            </p>
        </div>
        <div class="col-xs-12 col-sm-8 fondo-row-info">
            <br>
            <p class="resaltado-2 text-center">
                Envíenos sus datos y consulta y a la brevedad estaremos respondiéndole
            </p>
            <?php
            echo validation_errors("<div class='alert alert-danger'>","</div>");

            echo form_open_multipart(base_url('contacto/envio_correo'));

            echo '<div class="form-group">';
            echo form_label('Nombre y Apellido:', "nombre");
            echo form_input($campos['nombre']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Dirección de e-mail:', "correo");
            echo form_input($campos['correo']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Asunto:', "descripcion");
            echo form_input($campos['descripcion']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_label('Mensaje:', "contenido");
            echo form_textarea($campos['contenido']);
            echo '</div>';

            echo '<div class="form-group">';
            echo form_submit($campos['guardar']);
            echo '</div>';

            echo form_close();
            ?>
        </div>
    </div>
    <div class="row fondo-row-info">
        <div class="col-xs-12 fondo-row-info">
            <br>
            <img class="img-responsive center-block " src="<?php echo base_url()?>resources/img/logo_01.png" alt="Logo"/>
        </div>
        <div class="col-xs-offset-1 col-xs-10 col-md-offset-3 col-md-8 fondo-row-info">
            <p>Dirección: Avenida 20 entre calles 14 y 15, Edificio Manaure Nº 7-B, Barquisimeto, Edo. Lara, Venezuela <br>Teléfono local: +58 251 2406783 <br>Celular: +58 412 5150106 <img src="<?php echo base_url('resources/img/somos/').'iconowhatsap-16.png' ?>" alt="Icono de Whatsapp"><br>Email: contacto@geostecnologia.com.ve</p>
<!--            <p class="resaltado-2">Síguenos...</p>-->
        </div>
<!--
        <div class="col-xs-12 fondo-row-info">
            <div class="box">

                <ul class="share">

                    <li><a href="https://www.facebook.com/"><img src="<?php echo base_url()?>resources/img/facebook.png" alt="Facebook"/></a></li>
                    <li><a href="https://www.twitter.com/?lang=es"><img src="<?php echo base_url()?>resources/img/twiter.png" alt="Twitter"/></a></li>
    				<li><a href="https://www.instagram.com/?lang=es"><img src="<?php echo base_url()?>resources/img/instagram.png" alt="Instagram"/></a></li>

                </ul>
            </div>
-->
        </div>


    </div>
    <div class="row">
        <div class="col-xs-12"><br><br></div>
    </div>
</div>

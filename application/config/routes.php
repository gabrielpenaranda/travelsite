<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['usuario'] = 'usuario';
$route['admin'] = 'admin';
$route['verifica'] = 'admin/verifica';
$route['logout'] = 'admin/logout';
$route['admin/menu'] = 'admin/menu';
$route['destinos'] = 'destino';
$route['destinos/listado/(:any)'] = 'destino/listado/$1';
$route['destinos/info/(:any)'] = 'destino/info/$1';
$route['paquetes'] = 'paquete';
$route['paquetes/info/(:any)'] = 'paquete/info/$1';
$route['promociones'] = 'promocion';
$route['blog'] = 'blog';
$route['contacto'] = 'contacto';
$route['contacto/envio_correo'] = 'contacto/envio_correo';

$route['destinos-back'] = 'destino_back';
$route['destinos-back/registro'] = 'destino_back/registrar';
$route['destinos-back/elimina/(:any)'] = 'destino_back/eliminar/$1';
$route['destinos-back/edita/(:any)'] = 'destino_back/editar/$1';
$route['destinos-back/imagen/(:any)'] = 'destino_back/imagen/$1';
$route['destinos-back/cambia_imagen/(:any)'] = 'destino_back/cambiar_imagen/$1';
$route['destinos-back/editado/(:any)'] = 'destino_back/editado/$1';

$route['promociones-back'] = 'promocion_back';
$route['promociones-back/registro'] = 'promocion_back/registrar';
$route['promociones-back/elimina/(:any)'] = 'promocion_back/eliminar/$1';
$route['promociones-back/edita/(:any)'] = 'promocion_back/editar/$1';
$route['promociones-back/imagen/(:any)'] = 'promocion_back/imagen/$1';
$route['promociones-back/cambia_imagen/(:any)'] = 'promocion_back/cambiar_imagen/$1';
$route['promociones-back/editado/(:any)'] = 'promocion_back/editado/$1';

$route['paquetes-back'] = 'paquete_back';
$route['paquetes-back/registro'] = 'paquete_back/registrar';
$route['paquetes-back/elimina/(:any)'] = 'paquete_back/eliminar/$1';
$route['paquetes-back/edita/(:any)'] = 'paquete_back/editar/$1';
$route['paquetes-back/imagen/(:any)'] = 'paquete_back/imagen/$1';
$route['paquetes-back/cambia_imagen/(:any)'] = 'paquete_back/cambiar_imagen/$1';
$route['paquetes-back/editado/(:any)'] = 'paquete_back/editado/$1';

$route['blog-back'] = 'blog_back';
$route['blog-back/registro'] = 'blog_back/registrar';
$route['blog-back/elimina/(:any)'] = 'blog_back/eliminar/$1';
$route['blog-back/edita/(:any)'] = 'blog_back/editar/$1';
$route['blog-back/imagen/(:any)'] = 'blog_back/imagen/$1';
$route['blog-back/cambia_imagen/(:any)'] = 'blog_back/cambiar_imagen/$1';
$route['blog-back/editado/(:any)'] = 'blog_back/editado/$1';

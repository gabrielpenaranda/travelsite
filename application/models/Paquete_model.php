<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquete_model extends CI_Model {

	public function __construct(){
		parent::__construct();
  }

  public function obtener_paquetes()
  {
  	$r = $this->db->get('paquetes');
  	if ($r->num_rows() > 0)
  	{
  		return $r;
  	}
  	else
  	{
  		return NULL;
  	}
  }

  public function registrar_paquetes($dnombre,$ddescripcion,$dsiincluye,$dnoincluye, $dfecha_inicio,$dfecha_fin,$dprecio,$dstatus,$dimagen,$dthumb)
  {
  	$d = array(
  			'nombre'        =>  $dnombre,
  			'descripcion'   =>  $ddescripcion,
			'si_incluye'	=>	$dsiincluye,
			'no_incluye'	=>	$dnoincluye,
			'fecha_inicio'	=>	$dfecha_inicio,
			'fecha_fin'		=>	$dfecha_fin,
  			'imagen'        =>  $dimagen,
			'precio'		=>	$dprecio,
  			'thumb'			=>	$dthumb,
  			'status'		=>	$dstatus
  		 );
  	return $this->db->insert('paquetes', $d);
  }

  public function actualizar_paquetes($did,$dnombre,$ddescripcion,$dsiincluye,$dnoincluye, $dfecha_inicio,$dfecha_fin,$dprecio,$dstatus)
  {
  	$d = array(
  			'nombre'        =>  $dnombre,
  			'descripcion'   =>  $ddescripcion,
			'si_incluye'	=>	$dsiincluye,
			'no_incluye'	=>	$dnoincluye,
			'fecha_inicio'	=>	$dfecha_inicio,
			'fecha_fin'		=>	$dfecha_fin,
			'precio'		=>	$dprecio,
  			'status'		=>	$dstatus
  		 );
  	$this->db->where('id', $did);
  	return $this->db->update('paquetes', $d);
  }

  public function imagen_paquetes($did,$dimagen,$dthumb)
  {
  	$d = array(
  			'imagen'  =>  $dimagen,
  			'thumb'   =>  $dthumb,
  		 );
  	$this->db->where('id', $did);
  	return $this->db->update('paquetes', $d);
  }

  public function eliminar_paquetes($idd)
  {
  	$this->db->where('id',$idd);
  	$this->db->delete('paquetes');
  }

  public function editar_paquetes($idd)
  {
  	$this->db->where('id',$idd);
  	$r  = $this->db->get('paquetes');
  	// echo var_dump($r->row());
  	// exit;
  	return $r->row();
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model
{

    public function __construct()
    {
		parent::__construct();
    }

    public function obtiene_usuario()
    {
        return $this->db->get('usuarios');
    }

    public function verifica_usuario($u='')
    {
        $resultado = $this->db->query("SELECT * FROM usuarios WHERE usuario='".$u."' LIMIT 1;");

        if ($resultado->num_rows() > 0)
        {
            return $resultado->row();
        }
        else
        {
            return NULL;
        }
    }

}

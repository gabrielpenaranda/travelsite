<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocion_model extends CI_Model {

	public function __construct(){
		parent::__construct();
  }

  public function obtener_promociones()
  {
  	$r = $this->db->get('promociones');
  	if ($r->num_rows() > 0)
  	{
  		return $r;
  	}
  	else
  	{
  		return NULL;
  	}
  }

  public function registrar_promociones($dnombre,$ddescripcion,$dsiincluye,$dnoincluye, $dfecha_inicio,$dfecha_fin,$dprecio,$dstatus,$dimagen,$dthumb)
  {
  	$d = array(
  			'nombre'        =>  $dnombre,
  			'descripcion'   =>  $ddescripcion,
			'si_incluye'	=>	$dsiincluye,
			'no_incluye'	=>	$dnoincluye,
			'fecha_inicio'	=>	$dfecha_inicio,
			'fecha_fin'		=>	$dfecha_fin,
  			'imagen'        =>  $dimagen,
			'precio'		=>	$dprecio,
  			'thumb'			=>	$dthumb,
  			'status'		=>	$dstatus
  		 );
  	return $this->db->insert('promociones', $d);
  }

  public function actualizar_promociones($did,$dnombre,$ddescripcion,$dsiincluye,$dnoincluye, $dfecha_inicio,$dfecha_fin,$dprecio,$dstatus)
  {
  	$d = array(
  			'nombre'        =>  $dnombre,
  			'descripcion'   =>  $ddescripcion,
			'si_incluye'	=>	$dsiincluye,
			'no_incluye'	=>	$dnoincluye,
			'fecha_inicio'	=>	$dfecha_inicio,
			'fecha_fin'		=>	$dfecha_fin,
			'precio'		=>	$dprecio,
  			'status'		=>	$dstatus
  		 );
  	$this->db->where('id', $did);
  	return $this->db->update('promociones', $d);
  }

  public function imagen_promociones($did,$dimagen,$dthumb)
  {
  	$d = array(
  			'imagen'  =>  $dimagen,
  			'thumb'   =>  $dthumb,
  		 );
  	$this->db->where('id', $did);
  	return $this->db->update('promociones', $d);
  }

  public function eliminar_promociones($idd)
  {
  	$this->db->where('id',$idd);
  	$this->db->delete('promociones');
  }

  public function editar_promociones($idd)
  {
  	$this->db->where('id',$idd);
  	$r  = $this->db->get('promociones');
  	// echo var_dump($r->row());
  	// exit;
  	return $r->row();
  }

}

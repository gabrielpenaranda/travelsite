<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model
{

	public function __construct()
    {
		parent::__construct();
    }

    public function obtener_posts()
    {
        $r = $this->db->get('posts');
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

	public function numero_posts()
	{
		$r = $this->db->get('posts');
		return $r->num_rows();
	}

	public function obtener_posts_tipo($d)
    {
		$this->db->where('status','1');
		$this->db->where('tipo',$d);
        $r = $this->db->get('posts');
		// echo var_dump($r->result());
		// exit;
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

    public function registrar_posts($dnombre,$ddescripcion,$dimagen,$dcontenido,$dthumb)
    {
        $d = array(
                'nombre'        =>  $dnombre,
                'descripcion'   =>  $ddescripcion,
                'imagen'        =>  $dimagen,
				'contenido'		=>	$dcontenido,
				'thumb'			=>	$dthumb
             );
        return $this->db->insert('posts', $d);
    }

	public function actualizar_posts($did,$dnombre,$ddescripcion,$dcontenido)
    {
        $d = array(
                'nombre'        =>  $dnombre,
                'descripcion'   =>  $ddescripcion,
				'contenido'		=>	$dcontenido
             );
		$this->db->where('id', $did);
        return $this->db->update('posts', $d);
    }

	public function imagen_posts($did,$dimagen,$dthumb)
	{
		$d = array(
                'imagen'  =>  $dimagen,
                'thumb'   =>  $dthumb,
             );
		$this->db->where('id', $did);
        return $this->db->update('posts', $d);
	}

    public function eliminar_posts($id_post)
	{
        $this->db->where('id',$id_post);
        $this->db->delete('posts');
    }

	public function editar_posts($id_post)
	{
		$this->db->where('id',$id_post);
		$r  = $this->db->get('posts');
		// echo var_dump($r->row());
		// exit;
		return $r->row();
	}

}

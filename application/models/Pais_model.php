<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pais_model extends CI_Model {

    public function __construct() {
	parent::__construct();
    }

    public function get_paises() {
	   $this->db->order_by('descripcion');
	   $q = $this->db->get('pais');
	   if ($q->num_rows()>0){
            return $q;
	   }
	   else {
            return NULL;
	   }
    }

    public function grabar_pais($pais) {
        $d = array(
            "descripcion" => $pais
        );

        $this->db->insert('pais',$d);
    }

    public function elimina_pais($id_pais) {
        $this->db->where('id',$id_pais);
        $this->db->delete('pais');
    }
}

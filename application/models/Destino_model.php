<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destino_model extends CI_Model
{

	public function __construct()
    {
		parent::__construct();
    }

    public function obtener_destinos()
    {
        $r = $this->db->get('destinos');
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

	public function obtener_destinos_tipo($d)
    {
		$this->db->where('status','1');
		$this->db->where('tipo',$d);
        $r = $this->db->get('destinos');
		// echo var_dump($r->result());
		// exit;
        if ($r->num_rows() > 0)
        {
            return $r;
        }
        else
        {
            return NULL;
        }
    }

    public function registrar_destinos($dnombre,$ddescripcion,$dimagen,$dtipo,$dthumb,$dstatus)
    {
        $d = array(
                'nombre'        =>  $dnombre,
                'descripcion'   =>  $ddescripcion,
                'imagen'        =>  $dimagen,
				'tipo'			=>	$dtipo,
				'thumb'			=>	$dthumb,
				'status'		=>	$dstatus
             );
        return $this->db->insert('destinos', $d);
    }

	public function actualizar_destinos($did,$dnombre,$ddescripcion,$dtipo,$dstatus)
    {
        $d = array(
                'nombre'        =>  $dnombre,
                'descripcion'   =>  $ddescripcion,
				'tipo'			=>	$dtipo,
				'status'		=>	$dstatus
             );
		$this->db->where('id', $did);
        return $this->db->update('destinos', $d);
    }

	public function imagen_destinos($did,$dimagen,$dthumb)
	{
		$d = array(
                'imagen'  =>  $dimagen,
                'thumb'   =>  $dthumb,
             );
		$this->db->where('id', $did);
        return $this->db->update('destinos', $d);
	}

    public function eliminar_destinos($id_destino)
	{
        $this->db->where('id',$id_destino);
        $this->db->delete('destinos');
    }

	public function editar_destinos($id_destino)
	{
		$this->db->where('id',$id_destino);
		$r  = $this->db->get('destinos');
		// echo var_dump($r->row());
		// exit;
		return $r->row();
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocion extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Promocion_model', 'pr');
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $this->pr->obtener_promociones();
		$this->load->view('ctl/promociones/contenido', $data);
		$this->load->view('ctl/footer');
    }


	public function info($id)
	{
		$resultado = $this->pr->editar_promociones($id);
		$this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $resultado;
		$this->load->view('ctl/promociones/info',$data);
		$this->load->view('ctl/footer');
	}
}

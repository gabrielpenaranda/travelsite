<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destino extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Destino_model', 'dm');
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$this->load->view('ctl/destino/contenido-in');
		$this->load->view('ctl/footer');
    }

    public function listado($dest)
    {
		if ($dest == 1)
		{
			$dd = 'Nacional';
		}
		else
		{
			$dd = 'Internacional';
		}
		$resultado = $this->dm->obtener_destinos_tipo($dd);
//        if (!$resultado)
//		{
//			$error['error'] = 'No hay información disponible';
//			$error['ruta'] = 'destinos';
//			$this->_mensaje_error($error);
//		}
//		else
//		{
//		}
			$this->load->view('ctl/header');
			$this->load->view('ctl/nav');
			// $this->load->view('ctl/slider');
			$data['listado'] = $resultado;
			if ($dd == 'Nacional')
			{
				$data['titulo'] = 'DESTINOS NACIONALES';
			}
			else
			{
				$data['titulo'] = 'DESTINOS INTERNACIONALES';
			}
			$this->load->view('ctl/destino/contenido',$data);
			$this->load->view('ctl/footer');
    }

	public function info($id)
	{
		$resultado = $this->dm->editar_destinos($id);
		$this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $resultado;
		$this->load->view('ctl/destino/info',$data);
		$this->load->view('ctl/footer');
	}

	private function _mensaje($accion,$sino)
    {
        $this->load->view('ctl/header');
        $this->load->view('ctl/nav');
        if ($sino)
        {
            switch ($accion)
            {
                case 'registrar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué registrado!');
                    break;
                case 'eliminar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué eliminado!');
                    break;
                case 'editar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué modificado!');
                    break;
				case 'imagen':
					$datos = array('ruta' =>'destinos-back', 'mensaje' => 'La imágen fué modificada!');
					break;
            }
        }
        else
        {
            $datos = array('ruta' =>'destinos-back', 'mensaje' => 'Ha ocurrido un error, comuniquese con el administrador');
        }
        $this->load->view('ctl/mensaje', $datos);
    }

	private function _mensaje_error($merror)
	{
		$this->load->view('ctl/header');
        $this->load->view('ctl/nav');
		$this->load->view('ctl/mensaje-error', $merror);
	}

}

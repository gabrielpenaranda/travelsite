<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destino_back extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Destino_model', 'dm');
    }

    public function index()
    {
        if ($this->session->userdata('login'))
        {
            $data['listado'] = $this->dm->obtener_destinos();
            $data['campos'] = $this->_form_fields();
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/destino/contenido', $data);
        }
        else
        {
            redirect('admin');
        }
    }



    public function registrar()
    {
        $this->form_validation->set_rules('nombre','Destino','required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripción','required|xss_clean');
        // $this->form_validation->set_rules('imagen','Imágen','required|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
        if ($this->form_validation->run() == TRUE)
        {
            $config['upload_path']          = './uploads/images/destino/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 4096;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $config['overwrite']            = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('imagen')) {
                // $error = array('error' => $this->upload->display_errors());
				$error['error'] = $this->upload->display_errors();
				$error['ruta'] = 'destinos-back';
				$this->_mensaje_error($error);
            }
            else
            {
                //EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS
                //ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
                $file_info = $this->upload->data();
                //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
                //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
                $this->_create_thumbnail($file_info['file_name']);
                $data = array('upload_data' => $this->upload->data());
                $nombre = $this->input->post('nombre');
                $descripcion = $this->input->post('descripcion');
                $imagen = $file_info['file_name'];
				$tipo = $this->input->post('tipo');
				$thumb = $file_info['raw_name'].'_thumb'.$file_info['file_ext'];
				$status = $this->input->post('status');
                $subir = $this->dm->registrar_destinos($nombre,$descripcion,$imagen,$tipo,$thumb,$status);
				$this->_mensaje("registrar",$subir);
            }
        }
        else
        {
          //  echo validation_errors();
          //  exit;
            // $this->index();
			$this->index();
        }

        // redirect('destinos');
    }

    public function eliminar($id)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("destino/index");
        }
        else
        {
            $this->dm->eliminar_destinos($id);
            $this->_mensaje("eliminar", TRUE);
        }
    }

    public function editar($id)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("destino/index");
        }
        else
        {
			$data['listado'] = $this->dm->editar_destinos($id);
            $data['campos'] = $this->_form_fields();
			// $data['id'] = $id;
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/destino/contenido-edicion', $data);
        }
    }

	public function editado($id)
	{
		$this->form_validation->set_rules('nombre','Destino','required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('descripcion','Descripción','required|xss_clean');
        // $this->form_validation->set_rules('imagen','Imágen','required|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
        if ($this->form_validation->run() == TRUE)
        {
            $nombre = $this->input->post('nombre');
            $descripcion = $this->input->post('descripcion');
			$tipo = $this->input->post('tipo');
			$status = $this->input->post('status');
            $subir = $this->dm->actualizar_destinos($id,$nombre,$descripcion,$tipo,$status);
			$this->_mensaje("registrar",$subir);
        }
        else
        {
           	//  echo validation_errors();
           	//  exit;
           	// $this->index();
			$this->editar();
        }
	}

	public function imagen($id)
    {
        if (!$this->session->userdata('login'))
        {
            redirect("destino/index");
        }
        else
        {
			$data['listado'] = $this->dm->editar_destinos($id);
            $data['campos'] = $this->_form_fields();
			// $data['id'] = $id;
            $this->load->view('back/header');
            $this->load->view('back/nav');
            $this->load->view('back/destino/contenido-imagen', $data);
        }
    }

	public function cambiar_imagen($id)
	{
		$config['upload_path']          = './uploads/images/destino/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4096;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $config['overwrite']            = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('imagen')) {
            // $error = array('error' => $this->upload->display_errors());
			$error['error'] = $this->upload->display_errors();
			$error['ruta'] = 'destinos-back';
			$this->_mensaje_error($error);
        }
        else
        {
            //EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS
            //ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
            $file_info = $this->upload->data();
            //USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
            //ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
            $this->_create_thumbnail($file_info['file_name']);
            $data = array('upload_data' => $this->upload->data());
            $imagen = $file_info['file_name'];
			$thumb = $file_info['raw_name'].'_thumb'.$file_info['file_ext'];
            $subir = $this->dm->imagen_destinos($id,$imagen,$thumb);
			$this->_mensaje("imagen",$subir);
        }
	}

    private function _mensaje($accion,$sino)
    {
        $this->load->view('back/header');
        $this->load->view('back/nav');
        if ($sino)
        {
            switch ($accion)
            {
                case 'registrar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué registrado!');
                    break;
                case 'eliminar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué eliminado!');
                    break;
                case 'editar':
                    $datos = array('ruta' =>'destinos-back', 'mensaje' => 'El destino fué modificado!');
                    break;
				case 'imagen':
					$datos = array('ruta' =>'destinos-back', 'mensaje' => 'La imágen fué modificada!');
					break;
            }
        }
        else
        {
            $datos = array('ruta' =>'destinos-back', 'mensaje' => 'Ha ocurrido un error, comuniquese con el administrador');
        }
        $this->load->view('back/mensaje', $datos);
    }

	private function _mensaje_error($merror)
	{
		$this->load->view('back/header');
        $this->load->view('back/nav');
		$this->load->view('back/mensaje-error', $merror);
	}

    private function _form_fields()
    {
        $arreglo = array(
            'nombre'        => array(
                'name'          =>    'nombre',
                'id'            =>    'nombre',
                'value'         =>    set_value('nombre'),
                'placeholder'   =>    'Nombre del destino',
                'size'          =>    '50',
                'maxlenght'     =>    '100',
                'rows'          =>    '2',
                'class'         =>    'form-control'
            ),

            'descripcion'   => array(
                'name'          =>    'descripcion',
                'id'            =>    'descripcion',
                'value'         =>    set_value('descripcion'),
                'placeholder'   =>    'Descripción',
                'rows'          =>    '3',
                'class'         =>    'form-control'
            ),

			'tipo'			=> array(
				'name'			=>		'tipo',
				'options'		=>		array(
						'Nacional'		=>	'Nacional',
						'Internacional'	=>	'Internacional'
				),
				'selected'		=>		'Nacional',
				'id'			=>		'tipo',
				'class'         =>    	'form-control'
			),

			'status'			=> array(
				'name'			=>		'status',
				'options'		=>		array(
						'0'		=>	'Inactivo',
						'1'		=>	'Activo'
				),
				'selected'		=>		'1',
				'id'			=>		'status',
				'class'         =>    	'form-control'
			),

            'imagen'        => array(
                'name'          =>    'imagen',
                'id'            =>    'imagen',
//                'class'         =>    'form-control'
            ),

            'guardar'       => array(
                'name'          =>    'guardar',
                'id'            =>    'guardar',
                'value'         =>    'Guardar',
                'class'         =>    'btn btn-primary'
            )
        );

        return $arreglo;
    }

    private function _create_thumbnail($filename)
    {
        $config['image_library'] = 'gd2';
        //CARPETA EN LA QUE ESTÁ LA IMAGEN A REDIMENSIONAR
        $config['source_image'] = 'uploads/images/destino/'.$filename;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = FALSE;
        //CARPETA EN LA QUE GUARDAMOS LA MINIATURA
        $config['new_image']='uploads/thumbs/destino/';
        $config['width'] = 250;
        $config['height'] = 250;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

}

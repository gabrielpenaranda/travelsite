<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquete_back extends CI_Controller {

	public function __construct(){
    parent::__construct();
    $this->load->model('Paquete_model', 'pm');
  }
  public function index()
  {
  	if ($this->session->userdata('login'))
  	{
  		$data['listado'] = $this->pm->obtener_paquetes();
  		$data['campos'] = $this->_form_fields();
  		$this->load->view('back/header');
  		$this->load->view('back/nav');
  		$this->load->view('back/paquete/contenido', $data);
  	}
  	else
  	{
  		redirect('admin');
  	}
  }



  public function registrar()
  {
	//   echo $this->input->post('fecha_inicio').'<br>';
	//   echo $this->input->post('fecha_fin');
	//   exit;
  	$this->form_validation->set_rules('nombre','Paquete','required|max_length[100]|xss_clean');
  	$this->form_validation->set_rules('descripcion','Descripción','required|xss_clean');
	$this->form_validation->set_rules('siincluye','Incluye','required|xss_clean');
	$this->form_validation->set_rules('noincluye','No incluye','required|xss_clean');
	$this->form_validation->set_rules('fecha_inicio','Fecha Inicio','required|callback_verifica_fecha|xss_clean');
	$this->form_validation->set_rules('fecha_fin','Fecha Fin','required|callback_verifica_fecha|xss_clean');
	$this->form_validation->set_rules('precio','Precio','required|numeric|xss_clean');
  	// $this->form_validation->set_rules('imagen','Imágen','required|xss_clean');
  	$this->form_validation->set_message('required','%s es requerido');
  	if ($this->form_validation->run() == TRUE)
  	{
  		$config['upload_path']          = './uploads/images/paquete/';
  		$config['allowed_types']        = 'gif|jpg|png';
  		$config['max_size']             = 4096;
  		$config['max_width']            = 1024;
  		$config['max_height']           = 768;
  		$config['overwrite']            = TRUE;
  		$this->load->library('upload', $config);
  		if (!$this->upload->do_upload('imagen')) {
  			// $error = array('error' => $this->upload->display_errors());
  			$error['error'] = $this->upload->display_errors();
  			$error['ruta'] = 'paquetes-back';
  			$this->_mensaje_error($error);
  		}
  		else
  		{
  			//EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS
  			//ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
  			$file_info = $this->upload->data();
  			//USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
  			//ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
  			$this->_create_thumbnail($file_info['file_name']);
  			$data = array('upload_data' => $this->upload->data());
  			$nombre = $this->input->post('nombre');
  			$descripcion = $this->input->post('descripcion');
			$siincluye = $this->input->post('siincluye');
			$noincluye = $this->input->post('noincluye');
			$fecha = $this->input->post('fecha_inicio');
			$fecha_inicio = substr($fecha, 6, 4).'-'.substr($fecha, 3, 2).'-'.substr($fecha, 0, 2);
			// $ymd = substr($fecha, 3, 2).'-'.substr($fecha, 0, 2).'-'.substr($fecha, 6, 4);
			// $nfecha = strtotime($ymd);
			// $fecha_inicio = date('Y-M-d', $nfecha);
			// echo $fecha_inicio;
			// exit;
			// $ymd = DateTime::createFromFormat('d/m/Y', $fecha)->format('Y-m-d');
			$fecha = $this->input->post('fecha_fin');
			$fecha_fin = substr($fecha, 6, 4).'-'.substr($fecha, 3, 2).'-'.substr($fecha, 0, 2);
			$precio = $this->input->post('precio');
  			$imagen = $file_info['file_name'];
  			$tipo = $this->input->post('tipo');
  			$thumb = $file_info['raw_name'].'_thumb'.$file_info['file_ext'];
  			$status = $this->input->post('status');
  			$subir = $this->pm->registrar_paquetes($nombre,$descripcion,$siincluye,$noincluye,$fecha_inicio,$fecha_fin,$precio,$status,$imagen,$thumb);
  			$this->_mensaje("registrar",$subir);
  		}
  	}
  	else
  	{
  	  //  echo validation_errors();
  	  //  exit;
  		// $this->index();
  		$this->index();
  	}

  	// redirect('paquetes');
  }

  public function eliminar($id)
  {
  	if (!$this->session->userdata('login'))
  	{
  		redirect("paquete/index");
  	}
  	else
  	{
  		$this->pm->eliminar_paquetes($id);
  		$this->_mensaje("eliminar", TRUE);
  	}
  }

  public function editar($id)
  {
  	if (!$this->session->userdata('login'))
  	{
  		redirect("paquete/index");
  	}
  	else
  	{
  		$data['listado'] = $this->pm->editar_paquetes($id);
  		$data['campos'] = $this->_form_fields();
  		// $data['id'] = $id;
  		$this->load->view('back/header');
  		$this->load->view('back/nav');
  		$this->load->view('back/paquete/contenido-edicion', $data);
  	}
  }

  public function editado($id)
  {
	$this->form_validation->set_rules('nombre','Paquete','required|max_length[100]|xss_clean');
    $this->form_validation->set_rules('descripcion','Descripción','required|xss_clean');
  	$this->form_validation->set_rules('siincluye','Incluye','required|xss_clean');
  	$this->form_validation->set_rules('noincluye','No incluye','required|xss_clean');
  	$this->form_validation->set_rules('fecha_inicio','Fecha Inicio','required|callback_verifica_fecha|xss_clean');
  	$this->form_validation->set_rules('fecha_fin','Fecha Fin','required|callback_verifica_fecha|xss_clean');
  	$this->form_validation->set_rules('precio','Precio','required|numeric|xss_clean');
    $this->form_validation->set_message('required','%s es requerido');
  	if ($this->form_validation->run() == TRUE)
  	{
		$nombre = $this->input->post('nombre');
		$descripcion = $this->input->post('descripcion');
		$siincluye = $this->input->post('siincluye');
		$noincluye = $this->input->post('noincluye');
		$fecha = $this->input->post('fecha_inicio');
		$fecha_inicio = substr($fecha, 6, 4).'-'.substr($fecha, 3, 2).'-'.substr($fecha, 0, 2);
		// $ymd = substr($fecha, 3, 2).'-'.substr($fecha, 0, 2).'-'.substr($fecha, 6, 4);
		// $nfecha = strtotime($ymd);
		// $fecha_inicio = date('Y-M-d', $nfecha);
		// echo $fecha_inicio;
		// exit;
		// $ymd = DateTime::createFromFormat('d/m/Y', $fecha)->format('Y-m-d');
		$fecha = $this->input->post('fecha_fin');
		$fecha_fin = substr($fecha, 6, 4).'-'.substr($fecha, 3, 2).'-'.substr($fecha, 0, 2);
		$precio = $this->input->post('precio');
		$tipo = $this->input->post('tipo');
		$status = $this->input->post('status');
		$subir = $this->pm->actualizar_paquetes($id,$nombre,$descripcion,$siincluye,$noincluye,$fecha_inicio,$fecha_fin,$precio,$status);
  		$this->_mensaje("editar",$subir);
  	}
  	else
  	{
  		//  echo validation_errors();
  		//  exit;
  		// $this->index();
  		$this->editar();
  	}
  }

  public function imagen($id)
  {
  	if (!$this->session->userdata('login'))
  	{
  		redirect("paquete/index");
  	}
  	else
  	{
  		$data['listado'] = $this->pm->editar_paquetes($id);
  		$data['campos'] = $this->_form_fields();
  		// $data['id'] = $id;
  		$this->load->view('back/header');
  		$this->load->view('back/nav');
  		$this->load->view('back/paquete/contenido-imagen', $data);
  	}
  }

  public function cambiar_imagen($id)
  {
  	$config['upload_path']          = './uploads/images/paquete/';
  	$config['allowed_types']        = 'gif|jpg|png';
  	$config['max_size']             = 4096;
  	$config['max_width']            = 1024;
  	$config['max_height']           = 768;
  	$config['overwrite']            = TRUE;
  	$this->load->library('upload', $config);
  	if (!$this->upload->do_upload('imagen')) {
  		// $error = array('error' => $this->upload->display_errors());
  		$error['error'] = $this->upload->display_errors();
  		$error['ruta'] = 'paquetes-back';
  		$this->_mensaje_error($error);
  	}
  	else
  	{
  		//EN OTRO CASO SUBIMOS LA IMAGEN, CREAMOS LA MINIATURA Y HACEMOS
  		//ENVÍAMOS LOS DATOS AL MODELO PARA HACER LA INSERCIÓN
  		$file_info = $this->upload->data();
  		//USAMOS LA FUNCIÓN create_thumbnail Y LE PASAMOS EL NOMBRE DE LA IMAGEN,
  		//ASÍ YA TENEMOS LA IMAGEN REDIMENSIONADA
  		$this->_create_thumbnail($file_info['file_name']);
  		$data = array('upload_data' => $this->upload->data());
  		$imagen = $file_info['file_name'];
  		$thumb = $file_info['raw_name'].'_thumb'.$file_info['file_ext'];
  		$subir = $this->pm->imagen_paquetes($id,$imagen,$thumb);
  		$this->_mensaje("imagen",$subir);
  	}
  }

  	public function verifica_fecha($date)
	{
		if (preg_match('/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/', $date))
		{
			if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
				return true;
			else
				return false;
		}
		else
		{
			echo 'preg_match';
			exit;
			return false;
		}
	}

  	private function _mensaje($accion,$sino)
  	{
  		$this->load->view('back/header');
  		$this->load->view('back/nav');
  		if ($sino)
  		{
  			switch ($accion)
  			{
  				case 'registrar':
  					$datos = array('ruta' =>'paquetes-back', 'mensaje' => 'El paquete fué registrado!');
  					break;
  				case 'eliminar':
  					$datos = array('ruta' =>'paquetes-back', 'mensaje' => 'El paquete fué eliminado!');
  					break;
  				case 'editar':
  					$datos = array('ruta' =>'paquetes-back', 'mensaje' => 'El paquete fué modificado!');
  					break;
  				case 'imagen':
  					$datos = array('ruta' =>'paquetes-back', 'mensaje' => 'La imágen fué modificada!');
  					break;
  			}
  		}
  		else
  		{
	  			$datos = array('ruta' =>'paquetes-back', 'mensaje' => 'Ha ocurrido un error, comuniquese con el administrador');
	  	}
  		$this->load->view('back/mensaje', $datos);
  	}

private function _mensaje_error($merror)
  	{
  		$this->load->view('back/header');
  		$this->load->view('back/nav');
  		$this->load->view('back/mensaje-error', $merror);
  }

  	private function _form_fields()
  {
  	$arreglo = array(
  		'nombre'        => array(
  			'name'          =>    'nombre',
  			'id'            =>    'nombre',
  			'value'         =>    set_value('nombre'),
  			'placeholder'   =>    'Nombre del paquete',
  			'size'          =>    '50',
  			'maxlenght'     =>    '100',
            'rows'          =>    '2',
  			'class'         =>    'form-control'
  		),

  		'descripcion'   => array(
  			'name'          =>    'descripcion',
  			'id'            =>    'descripcion',
  			'value'         =>    set_value('descripcion'),
  			'placeholder'   =>    'Descripción',
  			'rows'          =>    '3',
  			'class'         =>    'form-control'
  		),

		'siincluye'   => array(
  			'name'          =>    'siincluye',
  			'id'            =>    'siincluye',
  			'value'         =>    set_value('siincluye'),
  			'placeholder'   =>    'Agregue lo incluido en el paquete',
  			'rows'          =>    '3',
  			'class'         =>    'form-control'
  		),

		'noincluye'   => array(
  			'name'          =>    'noincluye',
  			'id'            =>    'noincluye',
  			'value'         =>    set_value('noincluye'),
  			'placeholder'   =>    'Agregue lo no incluido en el paquete',
  			'rows'          =>    '3',
  			'class'         =>    'form-control'
  		),

		'precio'   => array(
  			'name'          =>    'precio',
  			'id'            =>    'precio',
  			'value'         =>    set_value('precio'),
  			'placeholder'   =>    '0,00',
  			'class'         =>    'form-control'
  		),

		'fecha_inicio'   => array(
  			'name'          =>    'fecha_inicio',
  			'id'            =>    'fecha_inicio',
  	// 		'value'         =>    set_value('precio'),
  			'class'         =>    'form-control datepicker'
  		),

		'fecha_fin'   => array(
  			'name'          =>    'fecha_fin',
  			'id'            =>    'fecha_fin',
  	// 		'value'         =>    set_value('precio'),
  			'class'         =>    'form-control datepicker'
  		),

  		'status'			=> array(
  			'name'			=>		'status',
  			'options'		=>		array(
  					'0'		=>	'Inactivo',
  					'1'		=>	'Activo'
  			),
  			'selected'		=>		'1',
  			'id'			=>		'status',
  			'class'         =>    	'form-control'
  		),

  		'imagen'        => array(
  			'name'          =>    'imagen',
  			'id'            =>    'imagen',
  //                'class'         =>    'form-control'
  		),

  		'guardar'       => array(
  			'name'          =>    'guardar',
  			'id'            =>    'guardar',
  			'value'         =>    'Guardar',
  			'class'         =>    'btn btn-primary'
  		)
  	);

  	return $arreglo;
  }

  private function _create_thumbnail($filename)
  {
  	$config['image_library'] = 'gd2';
  	//CARPETA EN LA QUE ESTÁ LA IMAGEN A REDIMENSIONAR
  	$config['source_image'] = 'uploads/images/paquete/'.$filename;
  	$config['create_thumb'] = TRUE;
  	$config['maintain_ratio'] = FALSE;
  	//CARPETA EN LA QUE GUARDAMOS LA MINIATURA
  	$config['new_image']='uploads/thumbs/paquete/';
  	$config['width'] = 250;
  	$config['height'] = 250;
  	$this->load->library('image_lib', $config);
  	$this->image_lib->resize();
  }

}

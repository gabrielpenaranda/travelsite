<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquete extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Paquete_model', 'pm');
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $this->pm->obtener_paquetes();
		$this->load->view('ctl/paquetes/contenido', $data);
		$this->load->view('ctl/footer');
    }


	public function info($id)
	{
		$resultado = $this->pm->editar_paquetes($id);
		$this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $resultado;
		$this->load->view('ctl/paquetes/info',$data);
		$this->load->view('ctl/footer');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Somos extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$this->load->view('ctl/somos/contenido');
		$this->load->view('ctl/footer');
    }


}

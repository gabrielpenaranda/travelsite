<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
		$this->load->library("email");
		$this->load->library("googlemaps");
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['campos'] = $this->_form_fields();
		$data['map'] = $this->mapa();
		$this->load->view('ctl/contacto/contenido', $data);
		$this->load->view('ctl/footer');
    }

public function mapa()
{
	//creamos la configuración del mapa con un array
	$config = array();
	//la zona del mapa que queremos mostrar al cargar el mapa
	//como vemos le podemos pasar la ciudad y el país
	//en lugar de la latitud y la longitud
	// $config['center'] = 'barquisimeto,venezuela';
	$config['center'] = '10.068975, -69.316974';
	// el zoom, que lo podemos poner en auto y de esa forma
	//siempre mostrará todos los markers ajustando el zoom
	$config['zoom'] = '6';
	//el tipo de mapa, en el pdf podéis ver más opciones
	$config['map_type'] = 'ROADMAP';
	//el ancho del mapa
	$config['map_width'] = '380px';
	//el alto del mapa
	$config['map_height'] = '497px';
	//inicializamos la configuración del mapa
	$this->googlemaps->initialize($config);

	//hacemos la consulta al modelo para pedirle
	//la posición de los markers y el infowindow

	//en $data['datos'tenemos la información de cada marker para
	//poder utilizarlo en el sidebar en nuestra vista mapa_view
	// $data['datos'] = $this->mapa_model->get_markers();
	//en data['map'] tenemos ya creado nuestro mapa para llamarlo en la vista
	return $this->googlemaps->create_map();
	// return $datos;
}

	public function envio_correo()
	{
		$this->form_validation->set_rules('nombre','Nombre y Apellido','required|max_length[50]|xss_clean');
		$this->form_validation->set_rules('correo','Dirección de e-mail','required|valid_email|max_length[50]|xss_clean');
        $this->form_validation->set_rules('descripcion','Asunto','required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('contenido','Mensaje','required|xss_clean');
        $this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('valid_email','%s no es valida');
		//configuracion para gmail
		$config_mail = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://sc5.conectarhosting.com',
			'smtp_port' => 465,
			'smtp_user' => 'grupocorturlifeca@turismodevida.com',
			'smtp_pass' => 'travelgctv2016',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		);
		if ($this->form_validation->run() == TRUE)
		{
			//cargamos la configuración para enviar con gmail
			$this->email->initialize($config_mail);

			$asunto = $this->input->post('descripcion');
			$mensaje = $this->input->post('nombre').' ('.$this->input->post('correo').') ha enviado el siguiente mensaje: '.$config_mail['newline'].''.$this->input->post('contenido');
			$this->email->from('grupocorturlifeca@turismodevida.com');
			$this->email->to('grupocorturlifeca@gmail.com');
			$this->email->subject($asunto);
			$this->email->message($mensaje);
			$this->email->send();
			// echo 'Asunto:   '.$asunto.'<br>';
			// echo 'Mensaje:  '.$mensaje.'<br>';
			// var_dump($this->email->print_debugger());
			// exit;
			$this->_mensaje();
		}
		else
		{
			$this->index();
		}
	}

	private function _mensaje()
    {
        $this->load->view('ctl/header');
        $this->load->view('ctl/nav');
		$datos = array('ruta' =>'contacto', 'mensaje' => 'Su mensaje fué enviado, le estaremos respondiendo a la brevedad posible!');
        $this->load->view('ctl/mensaje', $datos);
    }

	private function _form_fields()
    {
        $arreglo = array(
            'nombre'        => array(
                'name'          =>    'nombre',
                'id'            =>    'nombre',
                'value'         =>    set_value('nombre'),
                'placeholder'   =>    'Escriba su nombre completo',
                'size'          =>    '50',
                'maxlenght'     =>    '50',
                'class'         =>    'form-control'
            ),

			'correo'        => array(
                'name'          =>    'correo',
                'id'            =>    'correo',
                'value'         =>    set_value('correo'),
                'placeholder'   =>    'Escriba su dirección de e-mail',
                'size'          =>    '50',
                'maxlenght'     =>    '50',
                'class'         =>    'form-control'
            ),

            'descripcion'   => array(
                'name'          =>    'descripcion',
                'id'            =>    'descripcion',
                'value'         =>    set_value('descripcion'),
                'placeholder'   =>    'Asunto',
                'size'          =>    '50',
                'maxlenght'     =>    '50',
                'class'         =>    'form-control'
            ),

			'contenido'   => array(
                'name'          =>    'contenido',
                'id'            =>    'contenido',
                'value'         =>    set_value('contenido'),
                'placeholder'   =>    'Escriba su mensaje',
                'size'          =>    '50',
                'rows'  	    =>    '5',
                'class'         =>    'form-control'
            ),

            'guardar'       => array(
                'name'          =>    'guardar',
                'id'            =>    'guardar',
                'value'         =>    'Enviar',
                'class'         =>    'btn btn-primary'
            )
        );

        return $arreglo;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
    {
    parent::__construct();
    $this->load->model('Usuario_model');
    }

	public function index()
	{
		$this->load->view('back/header');
		$this->load->view('back/nav-nomenu');
		$this->load->view('back/login/content');
	}

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin');
    }

    public function verifica()
    {
        if ($this->input->post('usuario') != NULL and $this->input->post('password') != NULL)
        {
			$usuario = $this->input->post('usuario');
            $password = $this->input->post('password');
			$fila=$this->Usuario_model->verifica_usuario($usuario);
            if ($fila != NULL)
            {
				if ($fila->usuario === $usuario)
                {
					if ($fila->password === md5($password))
                    {
						$data = array(
						'usuario' => $usuario,
						'id'      => $fila->id,
						'login'   => TRUE );
						$this->session->set_userdata($data);
				        redirect('admin/menu',$data);
				    }
				    else
                    {
				        echo 'Password no coincide';
				    }
				}
				else
                {
				    echo 'Usuario no valido';
				}
            }
        }
        else
        {
          echo 'No ha introducido datos';
          redirect('admin');
        }
    }


	public function menu()
    {
		$this->load->view('back/header');
		$this->load->view('back/nav');
	}

}

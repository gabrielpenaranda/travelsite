<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Blog_model', 'bl');
    }

    public function index()
    {
        $this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $this->bl->obtener_posts();
		$data['numero'] = $this->bl->numero_posts();
		$this->load->view('ctl/blog/contenido', $data);
		$this->load->view('ctl/footer');
    }


	public function info($id)
	{
		$resultado = $this->bl->editar_posts($id);
		$this->load->view('ctl/header');
		$this->load->view('ctl/nav');
		// $this->load->view('ctl/slider');
		$data['listado'] = $resultado;
		$this->load->view('ctl/blog/info',$data);
		$this->load->view('ctl/footer');
	}
}
